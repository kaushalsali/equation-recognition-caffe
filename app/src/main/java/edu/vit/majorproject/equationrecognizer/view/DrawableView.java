package edu.vit.majorproject.equationrecognizer.view;

import edu.vit.majorproject.equationrecognizer.R;
import edu.vit.majorproject.equationrecognizer.activity.MainActivity;
import edu.vit.majorproject.equationrecognizer.model.Stroke;
import edu.vit.majorproject.equationrecognizer.model.Stroke.Point;
import edu.vit.majorproject.equationrecognizer.model.Character;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Debug;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


/**
 * Created by kaushal on 17/03/17.
 */

public class DrawableView extends View {
    private static final float STROKE_WIDTH = 5;
    private static final int PAINT_COLOR = Color.BLACK;
    public static Path path;
    private static Paint drawPaint;

    private static ArrayList<Stroke> strokes = new ArrayList<>();
    //public static ArrayList<Character> characters = new ArrayList<>();
    private static ArrayList<Point> temp_points = new ArrayList<>();

    public View v=findViewById(R.id.drawable_view);


/*
    public static List<Character> getCharacters(){
        return characters;
    }

    public static void clearCharacters() {
        characters = new ArrayList<>();
    }
*/
    public static List<Stroke> getStrokes() {
        return strokes;
    }

    public static void clearStrokes() {
        temp_points = new ArrayList<>();
        strokes = new ArrayList<>();
    }

    public DrawableView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setupPaint();
    }

    public static void setupPaint() {
        path = new Path();
        drawPaint = new Paint();
        drawPaint.setColor(PAINT_COLOR);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(STROKE_WIDTH);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPath(path, drawPaint);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int xCoordinate = (int)event.getX();
        int yCoordinate = (int)event.getY();
        System.out.println("-- " + xCoordinate +","+ yCoordinate  );
        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                path.moveTo(xCoordinate, yCoordinate);
                temp_points.add(new Point(xCoordinate,yCoordinate));
                //coordinates.add(new Point(xCoordinate, yCoordinate, event.getEventTime()));
                return true;

            case MotionEvent.ACTION_MOVE:
                path.lineTo(xCoordinate, yCoordinate);
                temp_points.add(new Point(xCoordinate,yCoordinate));
                //coordinates.add(new Point(xCoordinate, yCoordinate, event.getEventTime()));
                break;

            case MotionEvent.ACTION_UP:
                //coordinates.add(new Point(xCoordinate, yCoordinate, event.getEventTime()));
                temp_points.add(new Point(xCoordinate,yCoordinate));
                strokes.add(new Stroke.StrokeBuilder()
                        .withStrokeCoordinates(temp_points)
                        .createStroke());
                Log.d("DrawableView","stroke " + strokes.size() );
                temp_points = new ArrayList<>();
                break;

            default:
                return false;
        }
        postInvalidate();
        return true;
    }
}
