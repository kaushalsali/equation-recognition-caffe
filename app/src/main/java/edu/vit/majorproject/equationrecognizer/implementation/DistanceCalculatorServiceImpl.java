package edu.vit.majorproject.equationrecognizer.implementation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import edu.vit.majorproject.equationrecognizer.activity.MainActivity;
import edu.vit.majorproject.equationrecognizer.service.DistanceCalculatorService;

/**
 * Created by rahul on 30/4/17.
 */

public class DistanceCalculatorServiceImpl implements DistanceCalculatorService {
    Map<String, List<List<Double>>> map;

    @Override
    public List<String> calculateDistance(List<List<List<Double>>> list_of_character_DFTs)
    {
        System.out.println("-------------in-calculateDistance----------");
        map = MainActivity.centroid_map;
        List<String> predicted_character=new ArrayList<String>();
        int i=1;
        System.out.println("Number of characters in list = " + list_of_character_DFTs.size());
        for(List<List<Double>> test_character : list_of_character_DFTs) {  // for each character
            System.out.println("Character: " + i++);
            System.out.println("Number of strokes in character = " + test_character.size());
            System.out.println("----------------------");

            double minimum_key_distance = 999999999;
            String predicted_key = "";

            for (String key : map.keySet()) {   // for each key
                System.out.println("    For key: " + key);
                List<Double> minimum_stroke_distances = new ArrayList<>();
                List<List<Double>> centroid_list;
                centroid_list = map.get(key);

                int k = 1;
                for (List<Double> test_stroke : test_character) { // for each stroke
                    double test_stroke_distance = 999999999;

                    for (List<Double> single_centroid : centroid_list) { // for each centroid
                        double dist = euclideanDistance(test_stroke, single_centroid);
                        //System.out.println("Distance from stroke to centroid " + i++ + " = " + dist);
                        if (dist < test_stroke_distance) {
                            test_stroke_distance = dist;
                        }
                    }
                    System.out.println("        Minumum centroid distance for stroke " + k++ + " = " + test_stroke_distance);
                    minimum_stroke_distances.add(test_stroke_distance);
                }

                double test_character_distance = 0;    // Character distance from centroid
                for (Double d : minimum_stroke_distances) {
                    d = d * d;
                    test_character_distance = test_character_distance + d;
                }
                System.out.println("    Distance from stored character '" + key + "' = " + test_character_distance);
                System.out.println("    ------------");

                if (test_character_distance < minimum_key_distance) { //to keep track of minimum distance for key
                    minimum_key_distance = test_character_distance;
                    predicted_key = key;
                }
            }
            predicted_character.add(predicted_key);
            System.out.println("Predicted character: " + predicted_key);
            System.out.println("Character distance = " + minimum_key_distance);
            System.out.println("-----------------------");
        }
    return predicted_character;
    }

    private double euclideanDistance(List<Double> test_stroke, List<Double> centroid)
    {
        double sum = 0.0;
        for(int i=0; i<test_stroke.size(); i++) {
            sum = sum + Math.pow( test_stroke.get(i)-centroid.get(i), 2.0 );
        }
        //sum = Math.pow(sum,0.5);
        return Math.sqrt(sum);
    }
}
