package edu.vit.majorproject.equationrecognizer.service;

import java.util.List;

import edu.vit.majorproject.equationrecognizer.model.Character;
import edu.vit.majorproject.equationrecognizer.model.Stroke;

/**
 * Created by kaushal on 13/05/17.
 */

public interface SegmentationService {
    List<Character> segment(List<Stroke> strokes);
}
