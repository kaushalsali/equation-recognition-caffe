package edu.vit.majorproject.equationrecognizer.implementation;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.widget.TextView;

import java.util.*;

import edu.vit.majorproject.equationrecognizer.R;
import edu.vit.majorproject.equationrecognizer.activity.MainActivity;
import edu.vit.majorproject.equationrecognizer.service.EvaluatorService;

/**
 * Created by rahul on 14/5/17.
 */

public class EvaluatorServiceImpl implements EvaluatorService {


    @Override
    public void evaluate(List<String> eq) {
        //text_output = findViewById(R.id.text_output);
        String input="";
        try {
            for(int i=0; i<eq.size(); i++) {
                eq.set(i,eq.get(i).substring(6));
                input+=eq.get(i);
            }
            //Scanner scan = new Scanner(System.in);
        /* Create stacks for operators and operands */
            Stack<Integer> op  = new Stack<>();
            Stack<Double> val = new Stack<>();
        /* Create temporary stacks for operators and operands */
            Stack<Integer> optmp  = new Stack<>();
            Stack<Double> valtmp = new Stack<>();
        /* Accept expression */
            System.out.println("Evaluation Of Arithmetic Expression Using Stacks Test\n");
            System.out.println("Enter expression\n");
            //String input = scan.next();
            System.out.println("Input string is: " + input + " ");
            MainActivity.text_output.setText(input);
            input = "0" + input;
            input = input.replaceAll("-","+-");

        /* Store operands and operators in respective stacks */
            String temp = "";
            for (int i = 0;i < input.length();i++)
            {
                char ch = input.charAt(i);
                if (ch == '-')
                    temp = "-" + temp;
                else if (ch != '+' &&  ch != '*' && ch != '/')
                    temp = temp + ch;
                else
                {
                    val.push(Double.parseDouble(temp));
                    op.push((int)ch);
                    temp = "";
                }
            }
            val.push(Double.parseDouble(temp));
        /* Create char array of operators as per precedence */
        /* -ve sign is already taken care of while storing */
            char operators[] = {'/','*','+'};
        /* Evaluation of expression */
            for (int i = 0; i < 3; i++)
            {
                boolean it = false;
                while (!op.isEmpty())
                {
                    int optr = op.pop();
                    double v1 = val.pop();
                    double v2 = val.pop();
                    if (optr == operators[i])
                    {
                    /* if operator matches evaluate and store in temporary stack */
                        if (i == 0)
                        {
                            valtmp.push(v2 / v1);
                            it = true;
                            break;
                        }
                        else if (i == 1)
                        {
                            valtmp.push(v2 * v1);
                            it = true;
                            break;
                        }
                        else if (i == 2)
                        {
                            valtmp.push(v2 + v1);
                            it = true;
                            break;
                        }
                    }
                    else
                    {
                        valtmp.push(v1);
                        val.push(v2);
                        optmp.push(optr);
                    }
                }
            /* Push back all elements from temporary stacks to main stacks */
                while (!valtmp.isEmpty())
                    val.push(valtmp.pop());
                while (!optmp.isEmpty())
                    op.push(optmp.pop());

            /* Iterate again for same operator */
                if (it)
                    i--;
            }
            Double final_result = val.pop();
            System.out.println("\nResult = " + final_result);
            MainActivity.text_output.setText(MainActivity.text_output.getText() + " = " + final_result);
        }
        catch(Exception e)
        {
            System.out.println(input + " (Invalid Expression)");
            MainActivity.text_output.setText(input + " (Invalid Expression)");

        }
    }
}
