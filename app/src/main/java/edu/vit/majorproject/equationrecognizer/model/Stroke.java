package edu.vit.majorproject.equationrecognizer.model;

/**
 * Created by kaushal on 23/03/17.
 */

//import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by root on 9/3/16.
 */
public class Stroke implements Serializable {

    private List<Point> strokeCoordinates;

    public Stroke() {
    }

    private Stroke(StrokeBuilder strokeBuilder) {
        this.strokeCoordinates = strokeBuilder.strokeCoordinates;

    }

    public List<Point> getStrokeCoordinates() {
        return strokeCoordinates;
    }

    public void setStrokeCoordinates(List<Point> strokeCoordinates) {
        this.strokeCoordinates = strokeCoordinates;
    }

    public List<Point> prints()
    {
        return strokeCoordinates;
    }
    @Override
    public String toString() {
        return "Stroke{" +
                "strokeCoordinates=" + strokeCoordinates;
    }

    /**
     * Builder class for {@link Stroke}
     */
    public static class StrokeBuilder {
        private List<Point> strokeCoordinates;

        public StrokeBuilder withStrokeCoordinates(List<Point> strokeCoordinates) {
            this.strokeCoordinates = strokeCoordinates;
            return this;
        }


        public Stroke createStroke() {
            return new Stroke(this);
        }
    }

    public static class Point {
        //@JsonProperty("x")
        private int x;
        //@JsonProperty("y")
        private int y;
        //@JsonProperty
        //private long eventTime;

        public Point() {
        }
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
            //this.eventTime = eventTime;
        }
        public Point(int x, int y, long eventTime) {
            this.x = x;
            this.y = y;
            //this.eventTime = eventTime;
        }

        @Override
        public String toString() {
            return "(" + x + "," + y + ")";// + ", " + eventTime;
        }


        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }
    }
}
