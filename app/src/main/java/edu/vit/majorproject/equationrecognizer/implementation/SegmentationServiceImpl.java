package edu.vit.majorproject.equationrecognizer.implementation;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import edu.vit.majorproject.equationrecognizer.model.Character;
import edu.vit.majorproject.equationrecognizer.model.Stroke;
import edu.vit.majorproject.equationrecognizer.service.SegmentationService;

/**
 * Created by kaushal on 13/05/17.
 */

public class SegmentationServiceImpl implements SegmentationService {



    @Override
    public List<Character> segment(List<Stroke> strokes) {
        //System.out.println("----------------in-segment----------------");
        List<Character> characters = new ArrayList<>();
        boolean same_character = false;
        characters.add(new Character.CharacterBuilder()
                .withStroke(strokes.get(0))
                .createCharacter());
        //System.out.println(strokes.get(0).getStrokeCoordinates());

        for (int i = 1; i < strokes.size(); i++) {
            //System.out.println(strokes.get(i).getStrokeCoordinates());
            List<Stroke.Point> tempPoint = strokes.get(i).getStrokeCoordinates();
            List<Stroke.Point> tempPoint1 = strokes.get(i - 1).getStrokeCoordinates();

            for (Stroke.Point x : tempPoint) {
                int xPoint = x.getX();
                int yPoint = x.getY();
                for (Stroke.Point x1 : tempPoint1) {
                    int tempx = x1.getX();
                    int tempy = x1.getY();
                    //System.out.println("-----"+Math.abs(tempx - xPoint)+"-----"+Math.abs(tempy - yPoint));
                    if (Math.abs(tempx - xPoint) <= 25 && Math.abs(tempy - yPoint) <= 25) {
                        //System.out.println("-----"+tempx + ".........." + xPoint + "......." + tempy + "......." + yPoint);
                        //System.out.println("-----"+Math.abs(tempx - xPoint) + "-----" + Math.abs(tempy - yPoint));
                        Log.d("DrawableView","Strokes overlap");
                        same_character = true;
                        break;
                    }
                }

                if (same_character) // if current stroke belongs to the same character as that of previous stroke
                {
                    characters.set(characters.size() - 1, new Character.CharacterBuilder()
                            .withCharacterAndStroke(characters.get(characters.size() - 1), strokes.get(strokes.size() - 1))
                            .createCharacter());
                    break;
                }
            }

            if (!same_character) {
                Log.d("DrawableView","new character");
                characters.add(new Character.CharacterBuilder()
                        .withStroke(strokes.get(i))
                        .createCharacter());
            }
            same_character = false;
        }
        //System.out.println("Total characters and strokes:");
        System.out.println("\n---------total-characters----" + characters.size() + "------\n");
        System.out.println("\n---------total-strokes-------" + strokes.size() + "------\n");
        return characters;
    }
}
