package edu.vit.majorproject.equationrecognizer.implementation;

import java.util.ArrayList;
import java.util.List;
import android.util.Log;

import edu.vit.majorproject.equationrecognizer.R;
import edu.vit.majorproject.equationrecognizer.model.Character;
import edu.vit.majorproject.equationrecognizer.model.Stroke;
import edu.vit.majorproject.equationrecognizer.service.DFTService;
import edu.vit.majorproject.equationrecognizer.view.DrawableView;

/**
 * Created by kaushal on 30/04/17.
 */

public class DFTServiceImpl implements DFTService {

    /*
        * This implementation calculates DFTs for each stroke of a character separately.
        * Thus one character will have only one FD (which represents the whole character).
    */

    @Override
    public List<List<List<Double>>> calculateDFT(List<Character> characters, int ftpoints) {
        System.out.println("----------------in-calculateDFT----------------");
        List<List<Double>> character_DFTs;
        List<List<List<Double>>> list_of_character_DFTs = new ArrayList<>();

        int i=1;
        for(Character character : characters){
            Log.d("DFTServiceImpl", "Character: " + i++);
            character_DFTs = new ArrayList<>();

            int j=1;
            List<Stroke> strokes = character.getCharacterStrokes();
            for(Stroke stroke : strokes) {
                Log.d("DFTServiceImpl", "Stroke: " + j++);
                character_DFTs.add(dft(stroke, ftpoints));
            }
            list_of_character_DFTs.add(character_DFTs);
        }
        return list_of_character_DFTs;
    }

    private List<Double> dft(Stroke stroke, int ftpoints) {
        List<Stroke.Point> coordinates = stroke.getStrokeCoordinates();
        List<Double> dfts = new ArrayList<>();
        for (int i = 0; i < coordinates.size(); i++) {  // For each output element
            double real_sum = 0;
            double imag_sum = 0;
            for (int t = 0; t < coordinates.size(); t++) {  // For each input element
                double angle = 2 * Math.PI * t * i / coordinates.size();
                real_sum +=  coordinates.get(t).getX() * Math.cos(angle) + coordinates.get(t).getX() * Math.sin(angle);
                imag_sum += -coordinates.get(t).getY() * Math.sin(angle) + coordinates.get(t).getY() * Math.cos(angle);
            }
            dfts.add(real_sum);
            dfts.add(imag_sum);
        }
        dfts = dfts.subList(2, ftpoints*2+2); // 0th element skipped
        //System.out.println("-------DFTs--ftpoints-"+ftpoints+"----");
        for(Double each_dft:dfts){
            System.out.println(each_dft);
        }
        System.out.println("----------------------");

        return dfts;
    }

}
