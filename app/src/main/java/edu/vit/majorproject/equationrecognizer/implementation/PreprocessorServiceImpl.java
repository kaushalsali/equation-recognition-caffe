package edu.vit.majorproject.equationrecognizer.implementation;

import java.lang.*;
import java.util.*;

import edu.vit.majorproject.equationrecognizer.activity.MainActivity;
import edu.vit.majorproject.equationrecognizer.model.Character;
import edu.vit.majorproject.equationrecognizer.model.Stroke;
import edu.vit.majorproject.equationrecognizer.service.PreprocessorService;
import edu.vit.majorproject.equationrecognizer.view.DrawableView;

/**
 * Created by kaushal on 30/04/17.
 */

public class PreprocessorServiceImpl implements PreprocessorService {

    private int x_coord[], y_coord[];
    private int real, imag;

    @Override
    public void preprocess(List<Character> characters) {
        List<Stroke> strokes;
        for (Character character : characters) {
            strokes = character.getCharacterStrokes();
            for (Stroke stroke : strokes)
                rollingMean(stroke);
        }
    }

    private void rollingMean(Stroke stroke) {
        List<Stroke.Point> coordinates = stroke.getStrokeCoordinates();
        int maxWl = 20;
        for (int i = 0; i < coordinates.size(); i++) {
            int b = coordinates.size() - i - 1;
            if (i > 0 && b > 0) {
                int n = Math.min(i, Math.min(b, maxWl));
                int tempsum_x = 0;
                int tempsum_y = 0;
                int tempcount = 0;
                for (int j = (i - n); j < (i + n + 1); j++) {
                    tempsum_x += coordinates.get(j).getX();
                    tempsum_y += coordinates.get(j).getY();
                    tempcount++;
                }
                tempsum_x = tempsum_x / tempcount;
                tempsum_y = tempsum_y / tempcount;
                coordinates.set(i, new Stroke.Point(tempsum_x, tempsum_y));  // this should change the global variable--check--!!!
            }
        }
        System.out.println("--------------------After preprocessing----------------");
        for (int k = 0; k < coordinates.size(); k++) {
            //System.out.println("-local-coordinates--" + coordinates.get(k).getX() + ", " + coordinates.get(k).getY());
            //System.out.println("-global-coordinates--"+ MainActivity.characters.get(0).getCharacterStrokes().get(0).getStrokeCoordinates().get(k).getX() + ", " + coordinates.get(k).getY());
        }
    }

}
