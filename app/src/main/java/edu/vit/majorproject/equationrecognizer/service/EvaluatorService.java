package edu.vit.majorproject.equationrecognizer.service;

import java.util.List;

/**
 * Created by rahul on 14/5/17.
 */

public interface EvaluatorService {
    public void evaluate(List<String> predicted);
}
