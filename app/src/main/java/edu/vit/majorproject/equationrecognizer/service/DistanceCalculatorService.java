package edu.vit.majorproject.equationrecognizer.service;

import java.util.List;

/**
 * Created by rahul on 30/4/17.
 */

public interface DistanceCalculatorService {
    List<String> calculateDistance(List<List<List<Double>>> testSample);
}
