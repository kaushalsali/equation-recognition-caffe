package edu.vit.majorproject.equationrecognizer.service;

import java.util.List;
import edu.vit.majorproject.equationrecognizer.model.Character;

/**
 * Created by kaushal on 30/04/17.
 */

public interface PreprocessorService {
    void preprocess(List<Character> characters);
}
