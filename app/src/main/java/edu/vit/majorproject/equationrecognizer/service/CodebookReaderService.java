package edu.vit.majorproject.equationrecognizer.service;

import java.util.List;
import java.util.Map;

/**
 * Created by kaushal on 01/05/17.
 */

public interface CodebookReaderService {
    public Map<String, List<List<Double>>> getMap();

}
