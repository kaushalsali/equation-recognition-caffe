package edu.vit.majorproject.equationrecognizer.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kaushal on 23/03/17.
 */

public class Character {
    private List<Stroke> characterStrokes;

    public Character() {
    }

    private Character(CharacterBuilder characterBuilder) {
        this.characterStrokes = characterBuilder.strokes;
    }


    public List<Stroke> getCharacterStrokes() {
        return characterStrokes;
    }



    public static class CharacterBuilder {
        private List<Stroke> strokes;


        public CharacterBuilder withStroke(Stroke stroke) {
            this.strokes = new ArrayList<>();
            this.strokes.add(stroke);
            return this;
        }

        public CharacterBuilder withCharacterAndStroke(Character character, Stroke stroke) {
            this.strokes = new ArrayList<>();
            this.strokes.addAll(character.getCharacterStrokes());
            this.strokes.add(stroke);
            return this;
        }


        public Character createCharacter() {
            return new Character(this);
        }
    }


}