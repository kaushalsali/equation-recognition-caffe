package edu.vit.majorproject.equationrecognizer.activity;

import edu.vit.majorproject.equationrecognizer.R;
import edu.vit.majorproject.equationrecognizer.implementation.CodebookReaderServiceImpl;
import edu.vit.majorproject.equationrecognizer.implementation.DFTServiceImpl;
import edu.vit.majorproject.equationrecognizer.implementation.DFTServiceImplNew;
import edu.vit.majorproject.equationrecognizer.implementation.DistanceCalculatorServiceImpl;
import edu.vit.majorproject.equationrecognizer.implementation.EvaluatorServiceImpl;
import edu.vit.majorproject.equationrecognizer.implementation.PreprocessorServiceImpl;
import edu.vit.majorproject.equationrecognizer.implementation.SegmentationServiceImpl;
import edu.vit.majorproject.equationrecognizer.model.Character;
import edu.vit.majorproject.equationrecognizer.service.CodebookReaderService;
import edu.vit.majorproject.equationrecognizer.service.DFTService;
import edu.vit.majorproject.equationrecognizer.service.DistanceCalculatorService;
import edu.vit.majorproject.equationrecognizer.service.EvaluatorService;
import edu.vit.majorproject.equationrecognizer.service.PreprocessorService;
import edu.vit.majorproject.equationrecognizer.service.SegmentationService;
import edu.vit.majorproject.equationrecognizer.view.DrawableView;

import android.util.Log;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    CodebookReaderService codebook_reader_service;
    SegmentationService segmentation_service;
    PreprocessorService preprocessor_service;
    DFTService dft_service;
    DistanceCalculatorService distance_calculator_service;
    EvaluatorService evaluator_service;
    final int ftpoints = 6;

    public static Map<String, List<List<Double>>> centroid_map;
    public static List<Character> characters;
    List<List<List<Double>>> list_of_character_DFTs;
    public static TextView text_output;

    // Used to load the 'native-lib' library on application startup.
    static {System.loadLibrary("native-lib");}


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("MainActivity", "-----------App-Started------------\n----------------------------------\n");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        //TextView tv = (TextView) findViewById(R.id.sample_text);
        //tv.setText(stringFromJNI());

        try {
            InputStream codebook = getResources().openRawResource(R.raw.all_charwise_ft6_16_codebook);
            codebook_reader_service = new CodebookReaderServiceImpl(codebook);
        } catch (Exception e) {
           Log.e("MainActivity","Codebook Read Error",e);
        }
        //characters = new ArrayList<>();
        centroid_map = codebook_reader_service.getMap();
        segmentation_service = new SegmentationServiceImpl();
        preprocessor_service = new PreprocessorServiceImpl();

        // dft_service = new DFTServiceImpl();
        dft_service = new DFTServiceImplNew();
        distance_calculator_service = new DistanceCalculatorServiceImpl();
        evaluator_service = new EvaluatorServiceImpl();
        text_output = (TextView) findViewById(R.id.text_output);
    }



    public void clearDrawableView(View view) {
        Log.d("MainActivity","Cleared");
        System.out.println("-----------------Clear---------------\n-------------------------------------\n\n");
        findViewById(R.id.drawable_view).invalidate();
        DrawableView.clearStrokes();
        DrawableView.setupPaint();
        text_output.setText("");
        list_of_character_DFTs = new ArrayList<>();
        characters = new ArrayList<>();
    }

    public void processInput(View view) {
        List<String> predicted_character_list;
        list_of_character_DFTs = new ArrayList<>();
        characters = new ArrayList<>();

        if(DrawableView.getStrokes().get(0).getStrokeCoordinates().size() < ftpoints) {
            MainActivity.text_output.setText("Incomplete Character");
            return;
        }
        Log.d("MainActivity","Segmentation Started");
        characters = segmentation_service.segment(DrawableView.getStrokes());
        Log.d("MainActivity","Segmentation Done");

        Log.d("MainActivity","Preprocessing Started");
        preprocessor_service.preprocess(characters);
        Log.d("MainActivity","Preprocessing Done");

        Log.d("MainActivity","DFT Calculation Started");
        list_of_character_DFTs = dft_service.calculateDFT(characters, ftpoints);
        Log.d("MainActivity","DFT Calculation Done");

        Log.d("MainActivity", "Distance Calculation Started");
        predicted_character_list = distance_calculator_service.calculateDistance(list_of_character_DFTs);
        Log.d("MainActivity", "Distance Calculation Done\n");

        Log.d("MainActivity", "Final Evaluation Started");
        evaluator_service.evaluate(predicted_character_list);
        Log.d("MainActivity", "Final Evaluation Done\n");

    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI(); // not being recognized because of moving MainActivity to activity package
}
