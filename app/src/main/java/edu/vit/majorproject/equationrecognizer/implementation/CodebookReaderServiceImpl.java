package edu.vit.majorproject.equationrecognizer.implementation;

import java.util.List;
import java.util.Map;
import java.io.*;
import java.util.Scanner;
import java.util.LinkedHashMap;
import java.util.ArrayList;

import edu.vit.majorproject.equationrecognizer.service.CodebookReaderService;

/**
 * Created by kaushal on 01/05/17.
 */

public class CodebookReaderServiceImpl implements CodebookReaderService {

    Scanner in;
    Map<String, List<List<Double>>> map;


    public CodebookReaderServiceImpl(InputStream is) throws Exception {
        this.in = new Scanner(is);
        map = new LinkedHashMap<>();
    }


    CodebookReaderServiceImpl(String InputFile) throws Exception {
        this.in = new Scanner(new FileInputStream(InputFile));
        map = new LinkedHashMap<>();
    }


    @Override
    public Map<String, List<List<Double>>> getMap() {
        read();
        in.close();
        return map;
    }

    private void read() {
        String line = in.nextLine();
        String character = "";
        List<Double> centroidDimensions = null;
        List<List<Double>> centroid = null;
        for (int i = 0; ;++i) {
            if (line.contains("end") && !line.contains("end_character")) {
                break;
            } else if (line.contains("digit") || line.contains("operator")) {
                character = line;
                centroid = new ArrayList<>();
            } else if (line.contains("end_character")) {
                map.put(character, centroid);
                centroid = new ArrayList<>();
            } else if (line.contains("c")) {
                centroidDimensions = new ArrayList<>();
            } else if (line.contains("done")) {
                centroid.add(centroidDimensions);
                centroidDimensions = new ArrayList<>();
            } else {
                double num = Double.parseDouble(line);
                centroidDimensions.add(num);
            }
            line = in.nextLine();
        }

        // Usage
        //
        //
        //
        //
        //
        // delete this
        //for (List<Double> each_dim : map.get("digit_eight")) {
        //    for (double val : each_dim) {
        //        System.out.println(val);
        //    }
        //}
        //
        //
        //
        //
        // till here
    }

}
